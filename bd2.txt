-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema onlinemarket
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema onlinemarket
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `onlinemarket` DEFAULT CHARACTER SET utf8 ;
USE `onlinemarket` ;

-- -----------------------------------------------------
-- Table `onlinemarket`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `onlinemarket`.`product` (
  `idproduct` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL DEFAULT NULL,
  `stock` INT(11) NULL DEFAULT NULL,
  `description` VARCHAR(45) NULL DEFAULT NULL,
  `price` INT(11) NULL DEFAULT NULL,
  `label` VARCHAR(45) NULL DEFAULT NULL,
  `idUser` INT(11) NULL DEFAULT NULL,
  `quantity` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idproduct`),
  INDEX `idUser_idx` (`idUser` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `onlinemarket`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `onlinemarket`.`user` (
  `iduser` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(45) NULL DEFAULT NULL,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  `fund` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`iduser`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `onlinemarket`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `onlinemarket`.`order` (
  `idorder` INT(11) NOT NULL AUTO_INCREMENT,
  `idUser` INT(11) NULL DEFAULT NULL,
  `idProdus` INT(11) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `quantity` INT(11) NULL DEFAULT NULL,
  `price` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idorder`),
  INDEX `idUser_idx` (`idUser` ASC),
  INDEX `idProduct_idx` (`idProdus` ASC),
  CONSTRAINT `idProduct`
    FOREIGN KEY (`idProdus`)
    REFERENCES `onlinemarket`.`product` (`idproduct`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idUser`
    FOREIGN KEY (`idUser`)
    REFERENCES `onlinemarket`.`user` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
