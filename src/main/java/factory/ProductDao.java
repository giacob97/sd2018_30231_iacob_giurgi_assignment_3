package factory;

import model.Product;
import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class ProductDao implements DaoInterface<Product> {

    private SessionFactory sessionFactory;
    private HibConnection hibernateConnection = new HibConnection();

    public List<Product> findAll() {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        List<Product> products = session.createCriteria(Product.class).list();
        session.close();
        sessionFactory.close();
        return products;
    }

    public Product insert(Product product) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        session.save(product);
        session.beginTransaction().commit();
        session.close();
        sessionFactory.close();
        return product;
    }

    public Product findById(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Product product = session.get(Product.class,id);
        session.close();
        sessionFactory.close();
        return product;
    }

    public Product update(Product product) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        session.update(product);
        session.beginTransaction().commit();
        session.close();
        sessionFactory.close();
        return product;
    }

    public Product delete(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Product product = this.findById(id);
        session.delete(product);
        session.beginTransaction().commit();
        session.close();
        sessionFactory.close();
        return product;
    }

    public Product findByName(String name) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Product product = (Product)session.createCriteria(Product.class).add(Restrictions.eq("title",name)).uniqueResult();
        session.close();
        sessionFactory.close();
        return product;
    }

    @Override
    public void updateAll(List<Product> toUpdate, int number) {

    }
}
