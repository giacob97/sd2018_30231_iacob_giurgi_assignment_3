package factory;

public class DaoFactory {

    public static DaoInterface getDao(String type){

        if(type.equals("User")){
            return new UserDao();
        }
        else{
            if(type.equals("Product")){
                return new ProductDao();
            }
            else{
                return new OrderDao();
            }
        }
    }

}
