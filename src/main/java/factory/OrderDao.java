package factory;

import model.Order;
import model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.stream.Collectors;

public class OrderDao implements DaoInterface<Order> {

    private SessionFactory sessionFactory;
    private HibConnection hibernateConnection = new HibConnection();

    public List<Order> findAll() {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        List<Order> orders = session.createCriteria(Order.class).list();
        session.close();
        sessionFactory.close();
        return orders;
    }

    public Order insert(Order order) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        session.save(order);
        session.beginTransaction().commit();
        session.close();
        sessionFactory.close();
        return order;
    }

    public Order findById(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Order order = session.get(Order.class,id);
        session.close();
        sessionFactory.close();
        return order;
    }

    public Order update(Order order) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(order);
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
        return order;
    }

    public Order delete(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Order order = this.findById(id);
        session.delete(order);
        session.beginTransaction().commit();
        session.close();
        sessionFactory.close();
        return order;
    }


    public List<Order> getShoppingCartForAUser(int id){
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        List<Order> userOrders = session.createCriteria(Order.class).add(Restrictions.eq("idUser",id)).list();
        session.close();
        sessionFactory.close();
        return userOrders.stream().filter(x -> x.getStatus().equals("sc")).collect(Collectors.toList());
    }

    public int getSumForShoppingCart(List<Order> orders){
        int sum = 0;
        for(Order o : orders){
            sum = sum + o.getPrice();
        }
        return sum;
    }

    public Order findByName(String name) {
        return null;
    }

    @Override
    public void updateAll(List<Order> toUpdate, int number) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        for(Order order:toUpdate){
            System.out.println(order.getStatus());
            order.setStatus("paid");
            session.update(order);
            session.beginTransaction().commit();
        }

        session.close();
        sessionFactory.close();
    }
}
