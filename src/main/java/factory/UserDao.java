package factory;
import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class UserDao implements DaoInterface<User> {

    private SessionFactory sessionFactory;
    private HibConnection hibernateConnection = new HibConnection();

    public List<User> findAll() {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        List<User> users = session.createCriteria(User.class).list();
        session.close();
        sessionFactory.close();
        return users;
    }

    public User insert(User user) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        session.save(user);
        session.beginTransaction().commit();
        session.close();
        sessionFactory.close();
        return user;
    }

    public User findById(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        User user = session.get(User.class,id);
        session.close();
        sessionFactory.close();
        return user;
    }

        public User update(User user) {
            sessionFactory = hibernateConnection.initialize();
            Session session = sessionFactory.openSession();
            session.update(user);
            session.beginTransaction().commit();
            session.close();
            sessionFactory.close();
            return user;
    }

    public User delete(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        User user = this.findById(id);
        session.delete(user);
        session.beginTransaction().commit();
        session.close();
        sessionFactory.close();
        return null;
    }

    public User findByName(String name) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        try {
            User user = (User) session.createCriteria(User.class).add(Restrictions.eq("username", name)).uniqueResult();
            session.close();
            sessionFactory.close();
            return user;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public void updateAll(List<User> toUpdate,int nr) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        for(User user:toUpdate){
            int currentFund = user.getFund();
            user.setFund(currentFund + nr);
            session.update(user);
            session.beginTransaction().commit();
        }
        session.close();
        sessionFactory.close();
    }
}
