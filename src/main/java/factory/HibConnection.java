package factory;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibConnection {

    public SessionFactory initialize() {
        SessionFactory sessionFactory=new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
        return sessionFactory;
    }
}
