package factory;
import java.util.List;

public interface DaoInterface<T> {


    public List<T> findAll();
    public T insert(T t);
    public T findById(int id);
    public T update(T t);
    public T delete(int id);
    public T findByName(String name);
    public void updateAll(List<T> toUpdate,int number);

}
