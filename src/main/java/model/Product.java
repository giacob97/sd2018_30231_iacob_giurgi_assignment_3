package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Observable;

@Entity
@Table(name = "product")
public class Product extends Observable{

    @Column(name = "idproduct")
    @Id
    private int idProduct;


    @Column(name = "idUser")
    private int idUser;

    @Column(name ="title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private int price;

    @Column(name = "label")
    private String label;

    public Product() {
    }

    public Product(int idUser,String title, String description, int quantity, int price, String label) {
        this.title = title;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
        this.label = label;
        this.idUser = idUser;
    }


    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = this.getQuantity() - quantity;
        setChanged();
        notifyObservers(quantity);

    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
