package model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

@Entity
@Table(name="user")
public class User implements Observer{

    @Column(name = "iduser")
    @Id
    private int idUser;


    @Column(name ="username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "fund")
    private int fund;

    @Column(name = "type")
    private String type;

    public User(String username, String password, int fund, String type) {
        this.username = username;
        this.password = password;
        this.fund = fund;
        this.type = type;
    }

    @Override
    public String toString(){
        return this.getUsername() + " " + this.getFund();}

    public User(){};

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getFund() {
        return fund;
    }

    public void setFund(int fund) {
        this.fund = fund;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void update(Observable o, Object arg) {
            String nameOfObject;
            if(o instanceof Product){
                Product soldProduct = (Product) o;
                System.out.println("Notificare spre seller:" + getUsername() + ". S-a vandut o cantitate de " + arg.toString() + " a produsului:" + soldProduct.getTitle());
                if(((Product) o).getQuantity() == 0) {
                    System.out.println("Notificare spre seller:" + getUsername() + ". Stocul de " + soldProduct.getTitle() + " este epuizat");
                }
            }
            if(o instanceof Order){
                String str = "Notificare spre buyer: " + getUsername() + ".S-a cumparat produsul: " + arg.toString();
                this.writeReports(str);
            }
    }


    public void writeReports(String str) {
        Path filePath = Paths.get("d:/.sem 6/ps/a3/" + getType().toUpperCase() + " " + getUsername() + ".txt");
        try {
            if (!Files.exists(filePath)) {
                Files.createFile(filePath);
            }
            Date d = new Date();
            String now = " la data de " + d.toString();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append(now);
            stringBuilder.append(System.lineSeparator());
            Files.write(filePath,stringBuilder.toString().getBytes(), StandardOpenOption.APPEND);

        }catch (Exception e){
            e.printStackTrace();
        }

    }



}
