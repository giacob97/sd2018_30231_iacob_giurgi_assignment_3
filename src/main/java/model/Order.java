package model;


import factory.ProductDao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Observable;

@Entity
@Table(name = "order_table")
public class Order extends Observable{


    @Column(name = "idorder")
    @Id
    private int idOrder;

    @Column(name = "idUser")
    private int idUser;

    @Column(name = "idProdus")
    private int idProduct;

    @Column(name = "statusOrder",length = 15)
    private String status;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private int price;




    public Order() {
    }

    public Order(int idUser, int idProduct, int quantity, int price, String status) {
        this.idUser = idUser;
        this.idProduct = idProduct;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
    }


    @Override
    public String toString(){
        return this.getNameOfProduct() + " in cantitate de " + this.quantity + " bucati la pretul de "+ this.getPrice() + " $";
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        setChanged();
        notifyObservers(this);
    }

    public String getNameOfProduct(){
        String str = new ProductDao().findById(this.getIdProduct()).getTitle();
        return str;
    }



}
