package controller;

import factory.OrderDao;
import factory.ProductDao;
import factory.UserDao;
import model.Order;
import model.Product;
import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;


@Controller
public class ShoppingCartController {


    private int sum;
    public static User user;
    //= new UserDao().findById(1);


    @RequestMapping(value = "/shoppingCart" , method = RequestMethod.GET)
    public ModelAndView viewShoppingCart(){
        OrderDao orderDao = new OrderDao();
        List<Order> myOrders = orderDao.getShoppingCartForAUser(this.user.getIdUser());
        if(myOrders.size() >0) {
            sum = orderDao.getSumForShoppingCart(myOrders);
            ModelAndView model = new ModelAndView("shoppingCart", "list", myOrders);
            model.addObject("username",user.getUsername());
            model.addObject("sum", sum);
            model.addObject("messageSum","Total price:" + sum + " $");
            return model;
        }
        else{
            ModelAndView model = new ModelAndView();
            model.addObject("username",user.getUsername());
            model.addObject("message1", "The shopping cart is empty");
            return model;
        }
    }

    @RequestMapping(value = "/shoppingCart" , method = RequestMethod.POST)
    public ModelAndView pay(@ModelAttribute("modelView") ModelAndView model){
        model = this.viewShoppingCart();
        if(this.validateSum()) {
            this.updateAllUsers();
            model = new ModelAndView();
            model.addObject("message", "The shopping cart is empty");
            return model;
        }
        else{
            model.addObject("message","Invalid sum");
            return model;
        }

    }

    public void updateAllUsers(){
        OrderDao orderDao = new OrderDao();
        ProductDao productDao = new ProductDao();
        UserDao userDao = new UserDao();
        List<Order> myOrders = orderDao.getShoppingCartForAUser(this.user.getIdUser());
        List<User> users = new ArrayList<>();
        for(Order order : myOrders){
            order.addObserver(this.user);
            Product product = productDao.findById(order.getIdProduct());
            User user = userDao.findById(product.getIdUser());
            user.setFund(user.getFund()+order.getPrice());
            userDao.update(user);
            order.setStatus("paid");
            orderDao.update(order);
        }
        user.setFund(user.getFund() - sum);
        userDao.update(this.user);
    }

    public boolean validateSum(){

        if(this.sum > this.user.getFund()){
            return false;
        }
        return true;
    }

}
