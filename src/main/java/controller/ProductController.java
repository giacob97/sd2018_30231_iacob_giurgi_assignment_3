package controller;


import factory.DaoFactory;
import factory.DaoInterface;
import factory.ProductDao;
import factory.UserDao;
import model.Order;
import model.Product;
import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.WebParam;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class ProductController {

    public static User user;

    DaoInterface daoInterface = DaoFactory.getDao("Order");
    DaoInterface daoInterfaceProduct = DaoFactory.getDao("Product");
    DaoInterface daoInterfaceUser = DaoFactory.getDao("User");

    @RequestMapping(value = "/viewProducts" , method = RequestMethod.GET)
    public ModelAndView viewProducts(){
        return new ModelAndView("viewProducts","list",this.generateProducts());
    }

    @RequestMapping(value = "/viewProducts" , method = RequestMethod.POST)
    public ModelAndView addToShoppingCart(@ModelAttribute("order") Order order){
        int idProdus = order.getIdProduct();
        Product product = new ProductDao().findById(idProdus);
        User seller = (User) daoInterfaceUser.findById(product.getIdUser());
        product.addObserver(seller);
        int price = product.getPrice() * order.getQuantity();
        order.setPrice(price);
        order.setStatus("sc");
        order.setIdUser(1);
        product.setQuantity(order.getQuantity());
        daoInterfaceProduct.update(product);
        daoInterface.insert(order);

        return new ModelAndView("viewProducts","list",this.generateProducts());
    }

    public List<Product> generateProducts(){
        DaoInterface productInterface = DaoFactory.getDao("Product");
        List<Product> products = productInterface.findAll();
        return  products.stream().filter(x -> x.getQuantity() > 0).collect(Collectors.toList());
    }
}
