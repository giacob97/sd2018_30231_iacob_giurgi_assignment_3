package controller;


import factory.DaoFactory;
import factory.DaoInterface;
import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller

public class WelcomeController {

    public static User user;

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String sayHello(ModelMap modelMap){
        User user = new User();
        modelMap.addAttribute("user",user);
        modelMap.addAttribute("message","Welcome from spring");
        return "welcome";
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.POST)
    public String sayHelloAgain(ModelMap model, @ModelAttribute("user") User user){
        String username = user.getUsername();
        String password = user.getPassword();
        boolean isValidUser = this.checkData(username,password);
        if(isValidUser){
            BuyerController.user = this.user;
            return "redirect:/homepageBuyer";
        }
        else{
            model.addAttribute("message","Utilizator/parola gresita");
            return "welcome";
        }
    }





    public boolean checkData(String username,String password){
        DaoInterface daoFactory = DaoFactory.getDao("User");
        User user = null;
        user = (User)daoFactory.findByName(username);

        if(user==null){
            return false;
        }
        else {
            this.user = user;
            if (user.getPassword().equals(password)) {
                return true;
            } else {
                return false;
            }

        }
    }


}
