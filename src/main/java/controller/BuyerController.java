package controller;


import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BuyerController {
    public static User user;

    @RequestMapping(value = "/homepageBuyer", method = RequestMethod.GET)
    public String seeUserOptions(ModelMap modelMap){
        modelMap.addAttribute("user",user);
        return "homepageBuyer";
    }

    @RequestMapping(params = "logout", method = RequestMethod.POST)
    public String LogoutUser(HttpServletRequest request) {
        WelcomeController.user = new User();
        return "redirect:/welcome";
    }

    @RequestMapping(params = "sc", method = RequestMethod.POST)
    public String ViewShoppingCart(HttpServletRequest request) {
        ShoppingCartController.user = this.user;
        return "redirect:/shoppingCart";
    }

    @RequestMapping(params = "view", method = RequestMethod.POST)
    public String ViewAvailableProducts(HttpServletRequest request) {
        ProductController.user = this.user;
        return "redirect:/viewProducts";
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
