<%--
  Created by IntelliJ IDEA.
  User: Iacob
  Date: 5/1/2018
  Time: 5:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Products Available</title>

</head>



<body>



<form:form method="POST" modelAttribute="order" model="user">
    <h3>WELCOME ${user.username}</h3>
<h1>Available products</h1>
<table id="table" border="2" width="70%" cellpadding="2" >
    <tr><th>Name</th><th>Label</th><th>Description</th><th>Price</th><th>Stock</th><th>Quantity</th><th>Add</th></tr>
    <c:forEach var="product" items="${list}">
        <tr>
            <form:form id="${product.idProduct}">
            <input type="hidden" value="${product.idProduct}" name="idProduct">
            <td>${product.title}</td>
            <td>${product.label}</td>
            <td>${product.description}</td>
            <td>${product.price}</td>
            <td>${product.quantity}</td>
            <td><input type="text" name="quantity"></td>
            <td>
                <input type='submit' value="Add" >
            </td>
            </form:form>

        </tr>
    </c:forEach>
</table>
</form:form>
</body>
</html>

