<%--
  Created by IntelliJ IDEA.
  User: Iacob
  Date: 5/4/2018
  Time: 11:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>Shopping Cart</title>

</head>



<body>



<form  method="post" modelAttribute="list" >
    <h3>WELCOME ${username}</h3>
    <h1>Shopping cart</h1>
    <form:form id="showMe" >
    <table id="table" border="2" width="40%" cellpadding="2" >
        <tr><th>Name</th><th>Quantity</th><th>Price</th></tr>
        <c:forEach var="order" items="${list}">
            <tr>
                <form:form id="${order.idOrder}">
                    <td>${order.nameOfProduct}</td>
                    <td>${order.quantity}</td>
                    <td>${order.price}</td>
                </form:form>
            </tr>
        </c:forEach>
    </table>

    <br/>${messageSum}<input type="submit" value="Pay" >
        <br/>
        <h3>${message1}</h3>
    <form:form method = "post">
        <h2>${message}</h2>
    </form:form>
    </form:form>
</form>


</body>
</html>
